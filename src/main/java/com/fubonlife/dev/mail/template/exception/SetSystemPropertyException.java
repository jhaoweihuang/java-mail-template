package com.fubonlife.dev.mail.template.exception;

public class SetSystemPropertyException extends RuntimeException {

	private static final long serialVersionUID = 7165935145233066828L;

	public SetSystemPropertyException(String message, Throwable cause) {
		super(message, cause);
	}

	public SetSystemPropertyException(String message) {
		super(message);
	}

}

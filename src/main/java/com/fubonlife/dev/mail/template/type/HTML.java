package com.fubonlife.dev.mail.template.type;

import com.fubonlife.dev.mail.template.exception.SetSystemPropertyException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class HTML {

    public HTML(String filename) {
        this.filename = filename;
        StringBuilder contentBuilder = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            String str;
            while ((str = in.readLine()) != null) {
                contentBuilder.append(str);
            }
            in.close();
        } catch (IOException e) {
            throw new SetSystemPropertyException(String.format("讀取%s檔案失敗", filename));
        }
        this.htmlText = contentBuilder.toString();
    }

    private final String htmlText;

    private final String filename;

    public String getHtmlText() {
        return htmlText;
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return this.filename;
    }

}

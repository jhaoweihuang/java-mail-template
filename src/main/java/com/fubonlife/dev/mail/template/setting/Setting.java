package com.fubonlife.dev.mail.template.setting;

import com.fubonlife.dev.mail.template.type.HTML;
import com.fubonlife.dev.mail.template.exception.SetSystemPropertyException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public interface Setting {

    default void set(String key, String value) throws SetSystemPropertyException {
        if (key == null || value == null) {
            throw new SetSystemPropertyException("設定檔格式錯誤");
        }
        try {
            Field field = this.getClass().getDeclaredField(key);
            field.setAccessible(true);
            Class<?> type = field.getType();
            if (String.class.isAssignableFrom(type)) {
                field.set(this, value);
            } else if (Boolean.class.isAssignableFrom(type)) {
                value = value.toLowerCase();
                field.set(this, "1".equals(value) || "true".equals(value));
            } else if (Set.class.isAssignableFrom(field.getType())) {
                String[] elements = value.split(",");
                field.set(this,
                    Arrays.stream(elements).map(String::trim).filter(s -> s.length() > 0).collect(Collectors.toSet()));
            } else if (HTML.class.isAssignableFrom(field.getType())) {
                HTML html = new HTML(value);
                field.set(this, html);
            } else {
                throw new SetSystemPropertyException("設定檔目前不支援格式: %s" + field.getType().getSimpleName());
            }
            System.out.printf(" 設定 %-8.8s = %-20.20s\t成功\n", key, field.get(this));
        } catch (NoSuchFieldException e) {
            System.out.printf(" 設定 %-8.8s = %-20.20s\t失敗\n", key, value);
            throw new SetSystemPropertyException("設定檔含有未知的參數: " + key, e);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            System.out.printf(" 設定 %-8.8s = %-20.20s\t失敗\n", key, value);
            e.printStackTrace();
            throw new SetSystemPropertyException("無法設定參數: " + key, e);
        }
    }

}

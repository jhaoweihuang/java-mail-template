package com.fubonlife.dev.mail.template;

import com.fubonlife.dev.mail.template.setting.MailSetting;
import com.fubonlife.dev.mail.template.setting.SMTPSetting;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;
import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class App {

    private static final Stack<Exception> exceptionStack = new Stack<>();

    private static final ExecutorService executor = Executors.newFixedThreadPool(3);

    private static CountDownLatch latch;

    public static void main(final String[] args) {
        do {
            try {
                Scanner input = new Scanner(System.in);
                System.out.println("請按下 enter 發送信件，或輸入 'q' 離開程式");
                final String next = input.nextLine();
                if (next.equals("q") || next.equals("Q")) {
                    break;
                }
                // 步驟 1: 讀取 SMTP mail server 設定檔
                final SMTPSetting SMTPSetting = readSettings();
                // 步驟 2: 讀取信件設定檔
                final MailSetting mailSetting = readMailSettings();

                if (SMTPSetting == null || mailSetting == null) {
                    printError(0);
                } else {
                    sendMail(SMTPSetting, mailSetting);
                }
            } catch (Throwable throwable) {
                System.out.println("程式發生嚴重錯誤: ");
                System.out.println();
                System.out.println(throwable.getMessage());
            }
        } while (true);

        System.out.println("程式結束");
    }

    private static boolean set(final String filename, final BiConsumer<String, String> consumer) {
        boolean success = true;
        try {
            final BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(filename)), StandardCharsets.UTF_8));
            String data;
            while ((data = reader.readLine()) != null) {
                data = data.trim();
                if (data.length() == 0 || data.startsWith("#")) {
                    continue;
                }
                String key = data.replaceFirst("([^=]+)=(.*)", "$1");
                String value = data.replaceFirst("([^=]+)=(.*)", "$2");
                try {
                    consumer.accept(key, value);
                } catch (final Exception e) {
                    success = false;
                    exceptionStack.push(e);
                }
            }
            reader.close();
            return success;
        } catch (final Exception e) {
            exceptionStack.push(e);
            return false;
        }
    }

    private static void printError(int i) {
        if (i == 0) {
            System.out.println(String.format("共有%d個錯誤!", exceptionStack.size()));
        }
        if (!exceptionStack.isEmpty()) {
            final Exception e = exceptionStack.pop();
            System.out.println(e.getMessage());
            System.out.println();
            printError(++i);
        }
    }

    // 步驟 1
    private static SMTPSetting readSettings() {
        System.out.println("開始讀取SMTP設定檔案!");
        final SMTPSetting smtpSetting = new SMTPSetting();
        final boolean success = set("smtp.txt", smtpSetting::set);
        System.out.println(String.format("讀取SMTP設定檔案%s!", success ? "完畢" : "失敗"));
        System.out.println();
        return success ? smtpSetting : null;
    }

    // 步驟 2
    private static MailSetting readMailSettings() {
        System.out.println("開始讀取寄信設定檔案!");
        final MailSetting mailSetting = new MailSetting();
        final boolean success = set("mail.txt", mailSetting::set);
        System.out.println(String.format("讀取寄信設定檔案%s!", success ? "完畢" : "失敗"));
        System.out.println();
        return success ? mailSetting : null;
    }

    private static void sendMail(final SMTPSetting smtpSetting, final MailSetting mailSetting) {
        final Properties props = new Properties();
        props.put("mail.smtp.host", smtpSetting.getHost());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", smtpSetting.getTls());
        props.put("mail.smtp.port", smtpSetting.getPort());

        // create Authenticator object to pass in Session.getInstance argument
        final Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpSetting.getUsername(), smtpSetting.getPassword());
            }
        };

        final Session session = Session.getInstance(props, auth);

        try {
            int length = mailSetting.getTo().size();
            CountDownLatch sendLatch = new CountDownLatch(length);
            AtomicInteger success = new AtomicInteger();
            AtomicInteger fail = new AtomicInteger();
            for (final String to : mailSetting.getTo()) {
                final MimeMessage message = new MimeMessage(session);
                // 設定 headers
                message.addHeader("Content-type", "text/HTML; charset=UTF-8");
                message.addHeader("format", "flowed");
                message.addHeader("Content-Transfer-Encoding", "8bit");
                // 設定收信人, 寄信人, 主旨, 日期, 信件內容
                message.setFrom(new InternetAddress(smtpSetting.getFromMail(), smtpSetting.getFrom()));
                message.setReplyTo(InternetAddress.parse(smtpSetting.getReplyMail(), false));
                message.setSubject(mailSetting.getSubject(), "UTF-8");
                message.setText(mailSetting.getHtml().getHtmlText(), "UTF-8", "html");
                message.setSentDate(new Date());
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
                System.out.printf("開始寄送至:%-30.30s\n", to);
                Runnable runnable = () -> {
                    try {
                        Transport.send(message);
                        success.incrementAndGet();
                        System.out.printf("O 成功寄送至:%-30.30s\n", to);
                    } catch (MessagingException e) {
                        fail.incrementAndGet();
                        System.out.printf("X 無法寄送至:%-30.30s\n", to);
                    }
                    sendLatch.countDown();
                };
                executor.submit(runnable);
            }
            System.out.println("\n處理中\n");
            sendLatch.await();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            System.out.printf("\n總共%d封信件，成功:%d封，失敗:%d封\n時間: %s\n\n", length, success.get(), fail.get(),
                sdf.format(new Date()));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}

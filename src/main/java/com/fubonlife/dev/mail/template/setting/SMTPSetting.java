package com.fubonlife.dev.mail.template.setting;

public class SMTPSetting implements Setting {

    private String host;

    private String port;

    private String username;

    private String password;

    private String from;

    private String fromMail = "no-reply@example.com";

    private String replyMail = "no-reply@example.com";

    private Boolean tls;

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFrom() {
        return from;
    }

    public String getFromMail() {
        return fromMail;
    }

    public String getReplyMail() {
        return replyMail;
    }

    public Boolean getTls() {
        return tls;
    }

}

package com.fubonlife.dev.mail.template.setting;

import com.fubonlife.dev.mail.template.type.HTML;
import java.util.Set;

public class MailSetting implements Setting {

    private String subject;

    private Set<String> to;

    private HTML html;

    public String getSubject() {
        return subject;
    }

    public Set<String> getTo() {
        return to;
    }

    public HTML getHtml() {
        return html;
    }

}

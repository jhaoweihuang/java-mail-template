## Email HTML 樣板測試程式

---

#### 使用說明:

1. 請下載 [sender.jar](/release/sender.jar)
2. 確認有 Java 執行環境
(Java Runtime Environment，版本 1.8.x 以上)
3. 在相同目錄下，新增必要的設定檔 `smtp.txt` 以及 `mail.txt` 
(檔案的詳細設定，請參考[SMTP設定檔詳細說明](#smtp-setting)與[MAIL設定檔詳細說明](#mail-setting))
4. 依照程式提示執行，前往收件夾查看收到的信

#### <a name="smtp-setting"></a>SMTP設定檔詳細說明:
- 可以使用 [mailtrap.io](https://mailtrap.io/) 或是 [Gmail](https://www.google.com/intl/zh-TW/gmail/about/#) 當作 mail server
- 使用 mailtrap.io 與 Gmail 的差異
    - mailtrap.io 不會真的將信件寄出，所以可以隨意輸入收件人
      Gmail 會真的寄送信件，寄送對象可能會找不到
    - Gmail 有安全設定等問題需要注意，設定較複雜

- 設定 **mailtrap.io**

    1. 前往 [mailtrap.io]()，依照流程登入或申請帳號
    ![img](./assets/mailtrap-01.png)
    ![img](./assets/mailtrap-02.png)
    2. 進入 Inbox 找到 SMTP 相關設定
    ![img](./assets/mailtrap-03.png)
    3. 將相對應的值複製到 `smpt.txt`
    4. 執行程式後，可以在收信夾看到信件
    ![img](./assets/mailtrap-04.png)
    5. 參考下方設定:
    ````properties
        # Host
        host=smtp.mailtrap.io
        # Port
        port=2525
        # Username
        username=你的帳號
        # Password
        password=你的密碼
        # 可以設定其他名字，方便辨識
        from=我
        # 保持設定 1
        tls=1
    ````
- 設定 **Gmail**

    1. 前往 Google 帳戶
    ![img](./assets/gmail-01.png)
    2. 開啟低安全性應用程式存取權
    ![img](./assets/gmail-02.png)
    ![img](./assets/gmail-03.png)
    3. 參考下方設定:
    ````properties
        # Host
        host=smtp.gmail.com
        # Port
        port=587
        # Username
        username=你的帳號
        # Password
        password=你的密碼
        # 可以設定其他名字，方便辨識
        from=我
        # 保持設定 1
        tls=1
    ````
- 若 Gmail 開啟了兩階段驗證(2-Step Verification)，需要額外申請一組應用程式專用的密碼
，忘記該密碼的話需重新申請

    1. 前往 Google 帳戶 (若沒有開啟兩階段驗證，則無法看到**應用程式密碼**的選項)
    ![img](./assets/2-StepVerification-01.png)
    2. 產生新的一組應用程式密碼
    ![img](./assets/2-StepVerification-02.png)
    ![img](./assets/2-StepVerification-03.png)
    3. 該畫面只會顯示一次，記得複製密碼，並貼在 `smtp.txt` 的 `password`
    ，若不小心關掉，請刪除並重新新增一次
    ![img](./assets/2-StepVerification-04.png)
    
#### <a name="mail-setting"></a>MAIL設定檔詳細說明:

- 參考下方設定:

````properties
# 信件主旨
subject=這是一封測試信件
# 收信人 (可用逗號隔開)
to=aaaa@mail.com, bbbb@mail.com, cccc@mail.com, dddd@mail.com
# HTML 檔案路徑位置
html=template.html
````
